/* Utilities for easier use of the trello API */

const insertCard = (card, cards, index) => {
    try {
        const cardsPos = cards.map(card => card.pos);
        let newCardPos, newCardsArray;

        if (index === 0) {
            // if at start, half index of first card
            newCardPos = cardsPos[0] / 2;
            card.changePos(newCardPos);

            newCardsArray = Array.from(cards);
            newCardsArray.unshift(card);
        } else if (index === cards.length) {
            // if at the end, add 16384
            const newCardPos = cardsPos[-1] + 16384;
            card.changePos(newCardPos);

            newCardsArray = Array.from(cards);
            newCardsArray.push(card);
        } else {
            // else average the two cards between
            const beforeCardPos = cards[index - 1].pos;
            const afterCardPos = cards[index].pos;

            newCardPos = (beforeCardPos + afterCardPos) / 2;
            card.changePos(newCardPos);

            newCardsArray = Array.from(cards);
            newCardsArray.splice(index, 0, card);
        }
        return newCardsArray;
    } catch (err) {
        logger(`Error at <Utils/insertCard> : ${err.message}`);
    }
};

const generateQueryFromObject = obj => {
    let query = "";
    Object.keys(obj)
        // this filter is required, as some values can be undefined in the data
        .filter(key => obj[key])
        .map(key => (query += `&${key}=${obj[key]}`));
    return query;
};

const logger = {
    error: (str = `%c Error: `, ...args) => {
        console.log(`%c ${str}`, "color:red", ...args);
    },
    success: (str = `%c Success: `, ...args) => {
        console.log(`%c ${str}`, "color:mediumaquamarine", ...args);
    }
};

export default {insertCard, generateQueryFromObject, logger};
