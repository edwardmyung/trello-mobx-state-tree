import utils from "../utils";

export const create = opts => {
    const {data} = opts;
    const query = utils.generateQueryFromObject(data);

    return window.fetch(
        `https://api.trello.com/1/cards?key=${process.env.TRELLO_KEY}&token=${
            process.env.TRELLO_TOKEN
        }${query}`,
        {
            method: "POST",
            mode: "cors"
        }
    );
};

export const update = opts => {
    const {idCard, data} = opts;
    const query = utils.generateQueryFromObject(data);

    return window.fetch(
        `https://trello.com/1/cards/${idCard}?key=${process.env.TRELLO_KEY}&token=${
            process.env.TRELLO_TOKEN
        }${query}`,
        {
            method: "PUT",
            mode: "cors"
        }
    );
};
