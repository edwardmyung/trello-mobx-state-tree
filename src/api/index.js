import * as Board from "./Board";
import * as List from "./List";
import * as Card from "./Card";

export default {
    List,
    Card,
    Board
};
