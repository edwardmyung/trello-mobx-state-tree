import utils from "../utils";

export const create = opts => {
    const {data} = opts;

    const query = utils.generateQueryFromObject(data);

    return window.fetch(
        `https://api.trello.com/1/lists?key=${process.env.TRELLO_KEY}&token=${
            process.env.TRELLO_TOKEN
        }${query}`,
        {
            method: "POST",
            mode: "cors"
        }
    );
};

export const getCards = opts => {
    const {listId} = opts;

    return window.fetch(
        `https://trello.com/1/lists/${listId}/cards?key=${process.env.TRELLO_KEY}&token=${
            process.env.TRELLO_TOKEN
        }`,
        {
            method: "GET",
            mode: "cors"
        }
    );
};

export const update = opts => {
    const {idList, data} = opts;

    let query = utils.generateQueryFromObject(data);
    console.log(idList, data);
    // Object.keys(data)
    //     .filter(key => data[key])
    //     .map(key => (query += `&${key}=${data[key]}`));

    return window.fetch(
        `https://trello.com/1/lists/${idList}?key=${process.env.TRELLO_KEY}&token=${
            process.env.TRELLO_TOKEN
        }${query}`,
        {
            method: "PUT",
            mode: "cors"
        }
    );
};
