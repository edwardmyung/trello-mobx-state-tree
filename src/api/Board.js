import utils from "../utils";

export const getLists = opts => {
    const {idBoard, data} = opts;

    const query = utils.generateQueryFromObject(data);

    return window.fetch(
        `https://trello.com/1/boards/${idBoard}/lists?key=${
            process.env.TRELLO_KEY
        }&token=${process.env.TRELLO_TOKEN}${query}`,
        {
            method: "GET",
            mode: "cors"
        }
    );
};
