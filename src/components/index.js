import {default as Card} from "./Card";
import {default as List} from "./List";
import {default as Navbar} from "./Navbar";
import {default as svg} from "./svg";
import {default as generic} from "./generic";
import * as styleApp from "./style";

export default {
    Card,
    List,
    Navbar,
    svg,
    styleApp,
    generic
};
