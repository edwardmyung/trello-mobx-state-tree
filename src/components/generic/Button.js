import React from "react";
import styled from "styled-components";

const Button = styled.div`
    background-color: #5aac44;
    box-shadow: #3f6f21 0 1px 0 0;
    display: inline-block;
    margin: 0 5px 0 0;
    padding: 8px 14px;
    border-radius: 3px;
    color: white;
    border-style: none;
    cursor: pointer;
    ${p => p.theme.typography.secondary.paragraph.bold}
`;

export default ({href = "#", onClick = null, children, ...rest}) => (
    <a href={href} onClick={onClick} {...rest}>
        <Button>{children}</Button>
    </a>
);
