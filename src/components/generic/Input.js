import React from "react";
import styled from "styled-components";

const Input = styled.input`
    width: 100%;
    border-radius: 3px;
    padding: 8px 12px;
    outline: solid 2px rgba(0, 0, 0, 0);
    border: solid 1px rgba(0, 0, 0, 0.1);
    border-radius: 3px;

    transition: outline 0.3s ease-in-out;

    &:focus {
        outline: solid 2px rgba(0, 0, 0, 0.3);
    }

    ${p => p.theme.typography.secondary.paragraph.regular}
`;

export default class extends React.Component {
    constructor() {
        super();
        this.inputRef = React.createRef();
        this.onKeyPress = this.onKeyPress.bind(this);
    }

    componentDidMount() {
        this.inputRef.current.focus();
    }

    onKeyPress(e) {
        const {value} = this.props;
        if (e.which === 13) {
            if (value.trim().length === 0) return;
            this.props.handleEnterPress();
        }
    }

    render() {
        const value =
            this.props.value.trim().length === 0
                ? this.props.value.trim()
                : this.props.value;

        return (
            <Input
                {...this.props}
                ref={this.inputRef}
                value={value}
                onKeyPress={this.onKeyPress}
            />
        );
    }
}
