import React from "react";
import styled from "styled-components";

const TextArea = styled.textarea`
    resize: none;
    width: 100%;
    height: 70px;
    padding: 10px 8px;
    border: none;
    border-radius: 3px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.18);
    outline: solid 3px rgba(0, 0, 0, 0);
    overflow-wrap: break-word;

    ${p => p.theme.typography.secondary.paragraph.regular}
`;

export default class extends React.Component {
    constructor() {
        super();
        this.textAreaRef = React.createRef();
        this.onKeyPress = this.onKeyPress.bind(this);
    }

    componentDidMount() {
        this.textAreaRef.current.focus();
    }

    onKeyPress(e) {
        const {value} = this.props;

        if (e.which === 13) {
            if (value.trim().length == 0) return;

            this.props.handleEnterPress();
        }
    }

    render() {
        const value =
            this.props.value.trim().length === 0
                ? this.props.value.trim()
                : this.props.value;

        return (
            <TextArea
                {...this.props}
                value={value}
                ref={this.textAreaRef}
                onKeyPress={this.onKeyPress}
            />
        );
    }
}
