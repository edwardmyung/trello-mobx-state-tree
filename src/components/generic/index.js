import {default as Button} from "./Button";
import {default as TextArea} from "./TextArea";
import {default as Input} from "./Input";

export default {Button, TextArea, Input};
