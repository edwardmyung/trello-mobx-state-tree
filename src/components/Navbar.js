import React from "react";
import {observer} from "mobx-react";
import {withRouter} from "react-router";
import {Link} from "react-router-dom";
import styled from "styled-components";

import AssetLogoSvg from "./svg/logo.js";

const Wrapper = styled.div`
    background: ${p => p.theme.theme.primary};
    position: fixed;
    height: 50px;
    width: 100%;

    display: flex;
    justify-content: space-between;
    align-items: center;
    z-index: 1;
`;

const WrapperChild = styled.div`
    flex: 1 1 33%;
    display: flex;
    justify-content: ${p => (p.justify ? p.justify : "flex-start")};
`;

const NavbarItem = styled.div`
    color: ${p => p.theme.colors.white};
    padding: 0px 15px;
`;

const Navbar = withRouter(() => (
    <Wrapper>
        <WrapperChild>
            <Link to="/boards">
                <NavbarItem>Boards</NavbarItem>
            </Link>
            <Link to="/account">
                <NavbarItem>Account</NavbarItem>
            </Link>
        </WrapperChild>
        <WrapperChild justify="center">
            <AssetLogoSvg width="30px" height="30px" style={{margin: "5px"}} />
        </WrapperChild>
        <WrapperChild justify="flex-end">
            <NavbarItem>Lol</NavbarItem>
        </WrapperChild>
    </Wrapper>
));

export default observer(Navbar);
