import React from "react";
import styled from "styled-components";
import {observer} from "mobx-react";
import styleGuidelines from "../style-guidelines";
import {Droppable, Draggable} from "react-beautiful-dnd";
import Card from "./Card";
import models from "../models";
import generic from "./generic";
import svg from "./svg";

const ListWrapper = styled.div`
    width: 300px;
    background-color: ${p => p.theme.colors.lightGreys.tone3};
    border-radius: 3px;
    box-sizing: border-box;
    display: flex;
    flex-direction: column;
    max-height: 100%;
    position: relative;
    white-space: normal;
    margin-right: 10px;
`;

const ListHeader = styled.div`
    padding: 10px 14px;
    ${styleGuidelines.typography.secondary.paragraph.bold}
`;

const ListContent = styled.div`
    flex: 1 1 auto;
    padding: 0px 5px 0px 10px;
    margin: 0px 5px 0px 0px;
    min-height: 0;
    overflow-x: hidden;
    overflow-y: auto;

    &::-webkit-scrollbar {
        height: 8px;
        width: 8px;
    }
    &::-webkit-scrollbar-track-piece {
        background: rgba(0, 0, 0, 0.06);
        border-radius: 8px;
    }

    &::-webkit-scrollbar-thumb {
        background: rgba(0, 0, 0, 0.1);
        border-radius: 8px;
    }
`;

const ListFooter = styled.div`
    padding: 10px;
    transition: background-color 0.3s ease-in-out;
    ${p =>
        p.hoverable &&
        `
            cursor :pointer;
            &:hover {
                background-color:${p.theme.colors.greys.tone5};   
            }`}
`;

const AddCardCtasWrapper = styled.div`
    display: flex;
    align-items: center;
`;

const AddCardCta = styled.div`
    display: flex;
    align-items: center;
`;

const AddCard = observer(
    class extends React.Component {
        constructor() {
            super();
            this.state = {
                card: models.Card.create({name: ""}),
                isEditing: false
            };
            this.toggleIsEditing = this.toggleIsEditing.bind(this);
            this.submit = this.submit.bind(this);
        }

        submit() {
            const {list} = this.props;
            list.addCard(this.state.card);
            this.setState({
                card: models.Card.create({name: ""})
            });
        }

        toggleIsEditing() {
            this.setState(
                {
                    isEditing: !this.state.isEditing
                },
                () => {
                    if (!this.state.isEditing) this.resetCard();
                }
            );
        }

        resetCard() {
            this.setState({
                card: models.Card.create({name: ""})
            });
        }

        render() {
            const cardName = this.state.card.toJSON().name;

            const form = (
                <React.Fragment>
                    <generic.TextArea
                        type="text"
                        placeholder="Enter a title for this card..."
                        value={cardName}
                        onChange={e => this.state.card.changeName(e.target.value)}
                        handleEnterPress={() => this.submit()}
                        style={{marginBottom: 5}}
                    />
                    <br />
                    <AddCardCtasWrapper>
                        <generic.Button onClick={() => this.submit()}>
                            Submit
                        </generic.Button>
                        <svg.IconTimes
                            onClick={() => this.toggleIsEditing()}
                            fill="black"
                            style={{cursor: "pointer"}}
                            width="30px"
                            height="30px">
                            Cancel
                        </svg.IconTimes>
                    </AddCardCtasWrapper>
                </React.Fragment>
            );
            const cta = (
                <AddCardCta onClick={() => this.toggleIsEditing()}>
                    <svg.IconPlus fill="black" style={{marginRight: 5}} />
                    Add a card
                </AddCardCta>
            );
            const body = this.state.isEditing ? form : cta;

            return <ListFooter hoverable={!this.state.isEditing}>{body}</ListFooter>;
        }
    }
);

const List = ({index, dndList, board, cardMap}) => {
    const {name, id} = dndList.list.toJSON();
    const {cardIds} = dndList;

    return (
        <Draggable draggableId={id} index={index}>
            {draggableProvided => (
                <Droppable droppableId={id} type="card">
                    {(droppableProvided, _droppableSnapshot) => {
                        return (
                            <div
                                data-identifier="card-droppable-area"
                                ref={droppableProvided.innerRef}
                                {...droppableProvided.droppableProps}
                                // isDraggingOver={droppableSnapshot.isDraggingOver} //---- Not sure if this is needed anymore
                            >
                                <ListWrapper
                                    ref={draggableProvided.innerRef}
                                    {...draggableProvided.draggableProps}>
                                    <ListHeader {...draggableProvided.dragHandleProps}>
                                        {name}{" "}
                                        <button onClick={() => board.removeList(id)}>
                                            Remove list
                                        </button>
                                    </ListHeader>

                                    <ListContent>
                                        {cardIds.map((cardId, index) => {
                                            let card = cardMap[cardId];
                                            return (
                                                <Card
                                                    key={cardId}
                                                    index={index}
                                                    card={card}
                                                />
                                            );
                                        })}

                                        {droppableProvided.placeholder}
                                    </ListContent>
                                    <AddCard list={dndList.list} />
                                </ListWrapper>
                            </div>
                        );
                    }}
                </Droppable>
            )}
        </Draggable>
    );
};

export default observer(List);
