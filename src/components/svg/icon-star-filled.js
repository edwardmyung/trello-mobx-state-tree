import React from "react";

export default ({width = "20px", height = "20px", fill = "white", ...rest}) => (
    <svg width={width} height={height} viewBox="0 0 150 150" {...rest}>
        <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <polygon
                fill={fill}
                points="151 58.0353982 109.983173 93.4007585 122.324519 146 75.5 117.994943 28.6754808 146 41.1983173 93.4007585 0 58.0353982 54.265625 53.3678887 75.5 4 96.734375 53.3678887"
            />
        </g>
    </svg>
);
