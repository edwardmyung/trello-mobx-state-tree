import React from "react";

export default ({width = "20px", height = "20px", fill = "white", ...rest}) => (
    <svg width={width} height={height} viewBox="0 0 150 150" {...rest}>
        <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <path
                d="M75,103.406639 L103.251969,120 L95.8267717,88.4190871 L121,66.6514523 L87.6771654,63.9751037 L75,34 L62.3228346,63.9751037 L29,66.6514523 L54.1732283,88.4190871 L46.7480315,120 L75,103.406639 Z M151,58.0353982 L109.983173,93.4007585 L122.324519,146 L75.5,117.994943 L28.6754808,146 L41.1983173,93.4007585 L0,58.0353982 L54.265625,53.3678887 L75.5,4 L96.734375,53.3678887 L151,58.0353982 Z"
                id="star_border---material"
                fill={fill}
            />
        </g>
    </svg>
);
