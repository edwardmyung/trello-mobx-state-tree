import {default as IconStarOutline} from "./icon-star-outline";
import {default as IconStarFilled} from "./icon-star-filled";
import {default as IconEllipsis} from "./icon-ellipsis";
import {default as IconPlus} from "./icon-plus";
import {default as IconTimes} from "./icon-times";
import {default as Logo} from "./logo";

export default {
    IconStarOutline,
    IconStarFilled,
    IconPlus,
    IconTimes,
    IconEllipsis,
    Logo
};
