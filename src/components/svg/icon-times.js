import React from "react";

export default ({width = "20px", height = "20px", fill = "white", ...rest}) => (
    <svg width={width} height={height} viewBox="0 0 150 150" {...rest}>
        <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <polygon
                fill={fill}
                points="111.2 46.319244 82.419244 75.1 111.2 103.880756 103.880756 111.2 75.1 82.419244 46.319244 111.2 39 103.880756 67.780756 75.1 39 46.319244 46.319244 39 75.1 67.780756 103.880756 39"
            />
        </g>
    </svg>
);
