import React from "react";

export default ({width = "20px", height = "20px", fill = "white", ...rest}) => (
    <svg width={width} height={height} viewBox="0 0 150 150" {...rest}>
        <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <polygon
                fill={fill}
                points="115.82 80.7512715 80.7512715 80.7512715 80.7512715 115.82 69.2487285 115.82 69.2487285 80.7512715 34.18 80.7512715 34.18 69.2487285 69.2487285 69.2487285 69.2487285 34.18 80.7512715 34.18 80.7512715 69.2487285 115.82 69.2487285"
            />
        </g>
    </svg>
);
