import styled from "styled-components";

export const AppWrapper = styled.div.attrs({
    "data-identifier": "styleApp.AppWrapper"
})`
    height: 100vh;
    display: flex;
    background: #fff;
`;

export const AppBody = styled.div.attrs({
    "data-identifier": "styleApp.AppBody"
})`
    display: flex;
    overflow-x: auto;
    flex: 1;
    margin: 50px auto 0px;
    width: 100%;
    align-items: flex-start;
`;

export const Container = styled.div.attrs({
    "data-identifier": "styleApp.Container"
})`
    width: 100%;
    max-width: 1000px;
    position: relative;
    margin: 0 auto;
`;
