import React from "react";
import styled from "styled-components";
import {Draggable} from "react-beautiful-dnd";
import {observer} from "mobx-react";
import svg from "./svg";

const Wrapper = styled.div`
    background: ${p => p.theme.colors.white};
    margin: 0px 0px 10px;
    padding: 5px;
    border-radius: 3px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.18);

    &:first-child {
        margin-top: 10px;
    }
`;

const Card = ({card, index}) => {
    return (
        <Draggable draggableId={card.id} index={index} type="card">
            {(provided, snapshot) => {
                return (
                    <Wrapper
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        ref={provided.innerRef}
                        isDragging={snapshot.isDragging}>
                        {card.name}
                    </Wrapper>
                );
            }}
        </Draggable>
    );
};

export default observer(Card);
