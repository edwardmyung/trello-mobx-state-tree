import React from "react";
import {Link} from "react-router-dom";
import styled from "styled-components";
import {observer} from "mobx-react";
import svg from "./svg";

const WrapperLink = styled(Link)`
    border: solid 1px rgba(0, 0, 0, 0.1);
    border-radius: 6px;
    font-size: 12px;
    width: 24%;
    margin-right: 1%;
    margin-top: 1%;
    padding: 12px;
    height: 130px;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    background: ${p => (p.imageurl ? `url(${p.imageurl})` : p.theme.theme.primary)};
    background-size: cover;
    color: ${p => p.theme.colors.white} !important;
    ${p => p.theme.typography.secondary.h3.bold}
`;

const StarOutlineWrapper = styled.div`
    display: none;
    ${WrapperLink}:hover & {
        display: block;
    }
`;

const HoverableIconStarOutline = () => {
    // Todo: onclick, this needs to call a PUT on the **Member** resource :S (NOT on board)
    return (
        <StarOutlineWrapper>
            <svg.IconStarOutline
                onClick={e => {
                    e.preventDefault();
                }}
            />
        </StarOutlineWrapper>
    );
};

const BoardPreview = ({board}) => {
    return (
        <WrapperLink
            key={board.id}
            to={`/board/${board.id}`}
            imageurl={board.backgroundImageScaled}>
            <div onClick={() => board.loadLists()}>{board.name}</div>
            <div>
                {board.starred ? <svg.IconStarFilled /> : <HoverableIconStarOutline />}
            </div>
        </WrapperLink>
    );
};

export default observer(BoardPreview);
