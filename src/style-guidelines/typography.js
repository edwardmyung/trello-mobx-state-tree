const PRIMARY_FONT_STACK = "'Poppins', sans-serif;";
const SECONDARY_FONT_STACK = "'Roboto', sans-serif;";

export default {
    primary: {
        h1: {},
        h2: {},
        h3: {
            regular: `
                font-size : 16px;
                font-weight : 400;
                font-family : ${PRIMARY_FONT_STACK};
            `,
            bold: `
                font-size : 16px;
                font-weight : 700;
                font-family : ${PRIMARY_FONT_STACK};
            `
        }
    },

    secondary: {
        h1: {},
        h2: {},
        h3: {
            regular: `
                font-size : 16px;
                font-weight : 400;
                font-family : ${SECONDARY_FONT_STACK};
            `,
            bold: `
                font-size : 16px;
                font-weight : 700;
                font-family : ${SECONDARY_FONT_STACK};
            `
        },
        paragraph: {
            regular: `
                font-size:14px;
                font-weight:400;
                font-family:${SECONDARY_FONT_STACK};
            `,
            bold: `
                font-size:14px;
                font-weight:700;
                font-family:${SECONDARY_FONT_STACK};
            `
        }
    }
};
