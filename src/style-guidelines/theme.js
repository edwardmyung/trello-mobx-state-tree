import colors from "./colors";

export default {
    primary: colors.indigos.tone2,
    secondary: colors.lightGreys.tone3
};
