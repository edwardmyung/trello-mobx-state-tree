export default {
    white: "#FFFFFF",
    black: "#000000",
    lightGreys: {
        tone1: "#CED8DF",
        tone2: "#D7E0E7",
        tone3: "#E1E8ED",
        tone4: "#EBF1F5",
        tone5: "#F5F8FA"
    },
    greys: {
        tone1: "#5C7080",
        tone2: "#738694",
        tone3: "#8A9BA8",
        tone4: "#A7B6C2",
        tone5: "#BFCCD6"
    },
    darkGreys: {
        tone1: "#182026",
        tone2: "#202B33",
        tone3: "#283742",
        tone4: "#30404D",
        tone5: "#394B59"
    },
    indigos: {
        tone1: "#04096A",
        tone2: "#3239B7",
        tone3: "#4066EA",
        tone4: "#4580E6",
        tone5: "#669EFF"
    }
};
