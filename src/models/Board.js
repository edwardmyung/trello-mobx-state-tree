import {types, flow, applySnapshot} from "mobx-state-tree";
import api from "../api";
import List from "./List";
import utils from "../utils";

export default types
    .model("Board", {
        id: types.identifier,
        name: types.string,
        starred: types.boolean,
        backgroundImageScaled: types.maybeNull(types.string),
        lists: types.optional(types.array(types.late(() => List)), []),
        shortUrl: types.string,
        closed: types.boolean,
        loading: false
    })
    .actions(self => {
        return {
            afterCreate() {
                self.loadLists();
            },

            loadLists: flow(function* loadLists() {
                self.loading = true;
                try {
                    const response = yield api.Board.getLists({
                        idBoard: self.id,
                        data: {
                            cards: "open",
                            card_fields: "name",
                            filter: "open",
                            fields: "name,pos"
                        }
                    });
                    let lists = yield response.json();
                    applySnapshot(self.lists, lists);
                    self.loading = false;
                } catch (err) {
                    utils.logger.error(
                        `Error at <Board.actions.loadLists> : ${err.message}`
                    );
                }
            }),

            addList: flow(function* addList(list) {
                // const temporarilyCreatedUuid = list.id;
                const {name} = list.toJSON();

                try {
                    // update remote
                    const response = yield api.List.create({
                        data: {
                            name: name,
                            idBoard: self.id,
                            pos: "bottom"
                        }
                    });

                    // update MST
                    // N.B.: do NOT use list.push as that always pushes at the end
                    // regardless of position
                    const newList = yield response.json();
                    const newLists = [...self.lists, newList].sort(
                        (list1, list2) => list1.pos - list2.pos
                    );
                    applySnapshot(self.lists, newLists);
                } catch (err) {
                    utils.logger.error(
                        `Error at <Board.actions.addList> : ${err.message}`
                    );
                }
            }),

            removeList: flow(function* removeList(idList) {
                try {
                    yield api.List.update({
                        idList,
                        data: {closed: true}
                    });

                    const newLists = self.lists.filter(list => list.id !== idList);
                    applySnapshot(self.lists, newLists);
                } catch (err) {
                    utils.logger.error(
                        `Error at <Board.actions.removeList> : ${err.message}`
                    );
                }
            }),

            reOrderLists(listIds) {
                const lists = self.lists;
                const listsPos = lists.map(list => list.pos);

                const newLists = listIds
                    .map(listId => {
                        return lists.find(list => list.id === listId);
                    })
                    .map((list, index) => {
                        list.pos = listsPos[index];
                        return list;
                    });

                newLists.map(
                    flow(function*(list) {
                        try {
                            // Todo: optimise by preventing this fetch for if positions stay the same
                            yield api.List.update({
                                idList: list.id,
                                data: {pos: list.pos}
                            });
                        } catch (err) {
                            utils.logger.error(
                                `Error at <Board.actions.reOrderLists> : ${err.message}`
                            );
                        }
                    })
                );

                applySnapshot(self.lists, newLists);
            }
        };
    });
