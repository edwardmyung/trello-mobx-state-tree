const uuidVersion4 = require("uuid/v4");

export const assignUUID = snapshot => {
    if (!snapshot.id) {
        return Object.assign({}, snapshot, {id: uuidVersion4()});
    } else {
        return Object.assign({}, snapshot);
    }
};
