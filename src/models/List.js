import {types, flow, applySnapshot, detach, getParent} from "mobx-state-tree";

import Card from "./Card";
import utils from "../utils";
import * as utilsModel from "./utils";
import api from "../api";

export default types
    .model("List", {
        id: types.identifier,
        name: types.string,
        cards: types.optional(types.array(types.late(() => Card)), []),
        pos: types.maybe(types.number)
    })
    .preProcessSnapshot(snapshot => utilsModel.assignUUID(snapshot))
    .actions(self => ({
        afterCreate() {
            self.loadCards();
        },

        loadCards: flow(function* loadCards() {
            if (self.cards.length > 0) {
                try {
                    const response = yield api.List.getCards({listId: self.id});
                    const cards = yield response.json();
                    applySnapshot(self.cards, cards);
                } catch (err) {
                    utils.logger.error(
                        `Error at <List.actions.loadCards> : ${err.message}`
                    );
                }
            } else {
                return null;
            }
        }),

        addCard: flow(function* addCard(card) {
            const {name} = card.toJSON();
            try {
                // update remote
                const response = yield api.Card.create({
                    data: {
                        idList: self.id,
                        name
                    }
                });

                // update MST
                // No need to apply snapshot here, as card always appends at the end
                const card = yield response.json();
                self.cards.push(card);
            } catch (err) {
                utils.logger.error(`Error at <List.actions.addCard> : ${err.message}`);
            }
        }),

        attachCard(card, index) {
            const newCards = utils.insertCard(card, self.cards, index);
            applySnapshot(self.cards, newCards);
        },

        detachCard(cardId) {
            const card = self.cards.find(card => card.id === cardId);
            return detach(card);
        },

        changeName(str) {
            return (self.name = str);
        },

        reOrderCardsInSameList(cardIds) {
            try {
                let cardsPos = self.cards.map(card => card.toJSON().pos);

                let newCards = cardIds
                    .map(cardId => self.cards.find(card => card.id === cardId))
                    .map((card, index) => {
                        card.pos = cardsPos[index];
                        return card;
                    });

                newCards.map(
                    flow(function*(card) {
                        yield api.Card.update({
                            idCard: card.id,
                            data: {pos: card.pos}
                        });
                    })
                );
                applySnapshot(self.cards, newCards);
            } catch (err) {
                utils.logger(`Error at <List.actions.reOrderCards> : ${err.msg}`);
            }
        },

        moveCardFromList(cardId, destinationListId, destinationIndex) {
            try {
                const pos = self.cards.map(card => card.pos);

                // update MST
                const card = self.detachCard(cardId);

                self.cards.map((card, index) => card.changePos(pos[index]));

                const destinationList = getParent(self, 1)
                    .toJSON()
                    .find(list => list.id === destinationListId);

                destinationList.attachCard(card, destinationIndex);

                // update remote
                self.cards.map(
                    flow(function*(card) {
                        yield api.Card.update({
                            idCard: card.id,
                            data: {
                                pos: card.pos,
                                idList: self.id
                            }
                        });
                    })
                );

                destinationList.cards.map(
                    flow(function*(card) {
                        yield api.Card.update({
                            idCard: card.id,
                            data: {
                                pos: card.pos,
                                idList: destinationList.id
                            }
                        });
                    })
                );
            } catch (err) {
                utils.logger.error(
                    `%c Error at <List.actions.moveCardFromList> : ${err.message}`
                );
            }
        }
    }));
