import {types} from "mobx-state-tree";
import * as utilsModel from "./utils";

export default types
    .model("Card", {
        id: types.identifier,
        name: types.string,
        desc: types.maybe(types.string),
        pos: types.maybe(types.number)
    })
    .preProcessSnapshot(snapshot => utilsModel.assignUUID(snapshot))
    .actions(self => ({
        changeName(string) {
            return (self.name = string);
        },
        changePos(float) {
            return (self.pos = float);
        }
    }));
