import {types, flow, applySnapshot} from "mobx-state-tree";
import Board from "./Board";
import utils from "../utils";

export default types
    .model("Store", {
        boards: types.array(Board),
        loading: true
    })
    .actions(self => {
        return {
            afterCreate() {
                self.load();
            },

            changeLoading(bool) {
                self.loading = bool;
            },

            load: flow(function* load() {
                self.changeLoading(true);

                try {
                    const response = yield window.fetch(
                        "https://api.trello.com/1/members/me/boards?key=921e3cfc4d4876f70e9b12c7b8cb0e84&token=fa0dcc9cfc1bab5bb8c35d260a64778b68176ada82b897513cfa751a7da70754"
                    );
                    let boards = yield response.json();
                    boards = boards.map(board => ({
                        ...board,
                        backgroundImageScaled:
                            board.prefs.backgroundImageScaled &&
                            board.prefs.backgroundImageScaled[1].url
                    }));

                    applySnapshot(self.boards, boards);
                    utils.logger.success(`Success at <Store.actions.load> :`, boards);

                    setTimeout(function() {
                        self.changeLoading(false);
                    }, 300);
                } catch (err) {
                    utils.logger.error(`Error at <Store.actions.load> : ${err.message}`);
                }
            })
        };
    });
