import {default as Store} from "./Store";
import {default as Board} from "./Board";
import {default as List} from "./List";
import {default as Card} from "./Card";

export default {
    Store,
    Board,
    List,
    Card
};
