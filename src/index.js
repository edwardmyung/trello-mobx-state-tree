import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

import models from "./models";

let initialState = {boards: []};

let store = (window.store = models.Store.create(initialState));

ReactDOM.render(<App store={store} />, document.getElementById("root"));
