import React, {Component} from "react";
import {observer} from "mobx-react";
import views from "./views";

import "@atlaskit/css-reset";
import styleGuidelines from "./style-guidelines";
import * as styleApp from "./components/style";

import {ThemeProvider} from "styled-components";
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom";
import Navbar from "./components/Navbar";

class App extends Component {
    render() {
        const {store} = this.props;

        return (
            <ThemeProvider theme={styleGuidelines}>
                <Router>
                    <styleApp.AppWrapper data-identifier="AppWrapper">
                        <Navbar />
                        <styleApp.AppBody data-identifier="AppBody">
                            <Switch>
                                <Route
                                    exact
                                    path="/"
                                    render={props => (
                                        <Redirect
                                            to={{
                                                pathname: "/boards",
                                                state: {from: props.location}
                                            }}
                                        />
                                    )}
                                />
                                <Route
                                    exact
                                    path="/boards"
                                    render={props => (
                                        <views.Boards {...props} store={store} />
                                    )}
                                />
                                <Route
                                    exact
                                    path="/board/:id"
                                    render={props => (
                                        <views.Board {...props} store={store} />
                                    )}
                                />
                                <Route
                                    exact
                                    path="/account"
                                    render={() => <views.Login />}
                                />
                            </Switch>
                        </styleApp.AppBody>
                    </styleApp.AppWrapper>
                </Router>
            </ThemeProvider>
        );
    }
}

export default observer(App);
