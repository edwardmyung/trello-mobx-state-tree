import {default as Board} from "./Board";
import {default as Boards} from "./Boards";
import {default as Login} from "./Login";

export default {
    Board,
    Boards,
    Login
};
