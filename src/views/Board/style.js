import styled from "styled-components";

export const FlexWrapper = styled.div`
    display: flex;
    padding: 10px;
    background: url(${p => p.backgroundSrc}) no-repeat center center fixed;
    background-size: cover;
`;

export const ListsWrapper = styled.div`
    display: flex;
    flex: 1;
    /* align-items: flex-start;  ----- THIS WAS THE LAYOUT BREAKING STYLE*/
`;
