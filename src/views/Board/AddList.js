import React from "react";
import {observer} from "mobx-react";

import styled from "styled-components";
import models from "../../models";
import components from "../../components";
const svg = components.svg;
const {Button, Input} = components.generic;

const Wrapper = styled.div`
    width: 300px;
    background-color: rgba(0, 0, 0, 0.2);
    border-radius: 3px;
    box-sizing: border-box;
    display: flex;
    flex-direction: column;
    max-height: 100%;
    position: relative;
    white-space: normal;
    margin-right: 10px;
    padding: 10px;
`;

const AddListCta = styled.div`
    border-radius: 5px;
    color: rgba(255, 255, 255, 0.75);
    display: flex;
    align-items: center;
    cursor: pointer;
    transition: color 0.3s ease-in-out;
    &:hover {
        color: rgba(255, 255, 255, 1);
    }
`;

const EditCtasWrapper = styled.div`
    display: flex;
    align-items: center;
`;

const Edit = observer(
    class Edit extends React.Component {
        render() {
            return (
                <React.Fragment>
                    <Input
                        name="listTitle"
                        type="text"
                        placeholder="Enter list title..."
                        value={this.props.list.name}
                        onChange={e => this.props.list.changeName(e.target.value)}
                        handleEnterPress={() => this.props.onAdd()}
                        style={{marginBottom: 8}}
                    />

                    <EditCtasWrapper>
                        <Button onClick={() => this.props.onAdd()}>Submit</Button>
                        <svg.IconTimes
                            onClick={() => this.props.onCancel()}
                            fill="black"
                            style={{cursor: "pointer"}}
                            width="30px"
                            height="30px"
                        />
                    </EditCtasWrapper>
                </React.Fragment>
            );
        }
    }
);

export default observer(
    class extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                list: models.List.create({name: ""}),
                isEditing: false
            };

            this.onAdd = this.onAdd.bind(this);
            this.toggleIsEditing = this.toggleIsEditing.bind(this);
        }

        onAdd() {
            const {board} = this.props;
            board.addList(this.state.list);
            this.setState({
                list: models.List.create({name: ""})
            });
        }

        resetList() {
            this.setState({
                list: models.List.create({name: ""})
            });
        }

        toggleIsEditing() {
            this.setState({isEditing: !this.state.isEditing}, () => {
                if (!this.state.isEditing) {
                    this.resetList();
                }
            });
        }

        render() {
            const cta = (
                <AddListCta onClick={() => this.toggleIsEditing()}>
                    <svg.IconPlus style={{marginRight: 5}} /> Add a list
                </AddListCta>
            );
            const form = (
                <Edit
                    list={this.state.list}
                    onAdd={this.onAdd}
                    onCancel={this.toggleIsEditing}
                />
            );
            const body = this.state.isEditing ? form : cta;

            return (
                // This <div/> is required to avoid stretching of <Wrapper/> here
                <div>
                    <Wrapper>{body}</Wrapper>
                </div>
            );
        }
    }
);
