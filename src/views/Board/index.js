import React from "react";
import {observer} from "mobx-react";
import {onSnapshot} from "mobx-state-tree";
import List from "../../components/List";
import {DragDropContext, Droppable} from "react-beautiful-dnd";

import * as styleLocal from "./style";
import AddList from "./AddList";

function generateInitialState(board = {}) {
    let dndCards = {},
        dndLists = {},
        dndListOrder = [];

    if (board.lists) {
        board.lists.forEach(list => {
            list.cards.toJSON().length > 0 &&
                list.cards.forEach(card => {
                    dndCards[card.toJSON().id] = card;
                });

            dndLists[list.id] = {
                list: list,
                cardIds: list.cards && list.cards.map(card => card.id)
            };

            dndListOrder.push(list.id);
        });
    }

    return {
        board,
        dndCards,
        dndLists,
        dndListOrder
    };
}

class Board extends React.Component {
    constructor(props) {
        super(props);

        let board = this.props.store.boards.find(
            board => board.id === this.props.match.params.id
        );

        this.state = generateInitialState(board);

        if (board) {
            onSnapshot(board, () => {
                this.setState(generateInitialState(board));
            });
        }

        this.onDragEnd = this.onDragEnd.bind(this);
    }

    onDragEnd(result) {
        const {source, destination, draggableId, type} = result;

        if (!destination) {
            return;
        }

        if (
            destination.droppableId === source.droppableId &&
            destination.index === source.index
        ) {
            return;
        }

        if (type === "list") {
            const newDndListOrder = Array.from(this.state.dndListOrder);
            newDndListOrder.splice(source.index, 1);
            newDndListOrder.splice(destination.index, 0, draggableId);

            const newState = {
                ...this.state,
                dndListOrder: newDndListOrder
            };

            this.setState(newState);
            this.state.board.reOrderLists(newState.dndListOrder);
        }

        if (type === "card") {
            if (source.droppableId === destination.droppableId) {
                // card is dropped onto the same list (column) at a different index
                const newCardIds = Array.from(
                    this.state.dndLists[source.droppableId].cardIds
                );

                newCardIds.splice(source.index, 1);
                newCardIds.splice(destination.index, 0, draggableId);

                // Update react state
                this.setState({
                    ...this.state,
                    dndLists: {
                        ...this.state.dndLists,
                        [source.droppableId]: {
                            ...this.state.dndLists[source.droppableId],
                            cardIds: newCardIds
                        }
                    }
                });

                // Update backend
                let listToUpdate = this.state.board.lists.find(
                    list => list.id === source.droppableId
                );
                listToUpdate.reOrderCardsInSameList(newCardIds);
            } else {
                // card is dropped onto a different column
                const newFromListCardIds = Array.from(
                    this.state.dndLists[source.droppableId].cardIds
                );
                newFromListCardIds.splice(source.index, 1);

                const newToListCardIds = Array.from(
                    this.state.dndLists[destination.droppableId].cardIds
                );
                newToListCardIds.splice(destination.index, 0, draggableId);

                // Update react state
                this.setState({
                    ...this.state,
                    dndLists: {
                        ...this.state.dndLists,
                        [source.droppableId]: {
                            ...this.state.dndLists[source.droppableId],
                            cardIds: newFromListCardIds
                        },
                        [destination.droppableId]: {
                            ...this.state.dndLists[destination.droppableId],
                            cardIds: newToListCardIds
                        }
                    }
                });

                // Update backend
                let sourceList = this.state.board.lists.find(
                    list => list.id === source.droppableId
                );

                sourceList.moveCardFromList(
                    draggableId,
                    destination.droppableId,
                    destination.index
                );
            }
        }
    }

    render() {
        return (
            <styleLocal.FlexWrapper
                backgroundSrc={this.state.board.backgroundImageScaled}>
                <DragDropContext onDragEnd={this.onDragEnd}>
                    <Droppable droppableId="all-lists" direction="horizontal" type="list">
                        {provided => {
                            return (
                                <styleLocal.ListsWrapper
                                    {...provided.droppableProps}
                                    ref={provided.innerRef}>
                                    {this.state.dndListOrder.map((listId, index) => {
                                        let dndList = this.state.dndLists[listId];
                                        return (
                                            <List
                                                key={dndList.list.id}
                                                dndList={dndList}
                                                board={this.state.board}
                                                index={index}
                                                cardMap={this.state.dndCards}
                                            />
                                        );
                                    })}

                                    {provided.placeholder}
                                </styleLocal.ListsWrapper>
                            );
                        }}
                    </Droppable>
                </DragDropContext>
                <AddList board={this.state.board} />
            </styleLocal.FlexWrapper>
        );
    }
}

export default observer(Board);
