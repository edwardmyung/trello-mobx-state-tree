import React from "react";
import {observer} from "mobx-react";
import BoardPreview from "../../components/BoardPreview";
import * as styleApp from "../../components/style";
import styled from "styled-components";

const BoardsWrapper = styled.div.attrs({"data-identifier": "BoardsWrapper"})`
    display: flex;
    flex-wrap: wrap;
`;

const Heading = styled.div`
    ${p => p.theme.typography.primary.h3.bold};
    margin-top: 20px;
`;

export default observer(props => {
    const {store} = props;

    const boards = store.boards.toJSON();
    const starredBoards = boards.filter(x => x.starred);
    const unstarredBoards = boards.filter(x => !x.starred);

    if (store.loading) {
        return <div>LOADING</div>;
    }

    return (
        // Currently, bug with padding bottom not working, is to do with the flex css of the parent of styleApp.Container
        <styleApp.Container style={{padding: "10px 20px 30px"}}>
            <Heading>Starred</Heading>

            <BoardsWrapper>
                {starredBoards.map(board => {
                    if (board.closed) return null;
                    return <BoardPreview key={board.id} board={board} />;
                })}
            </BoardsWrapper>

            <Heading>Unstarred</Heading>
            <BoardsWrapper>
                {unstarredBoards.map(board => {
                    if (board.closed) return null;
                    return <BoardPreview key={board.id} board={board} />;
                })}
            </BoardsWrapper>
        </styleApp.Container>
    );
});
