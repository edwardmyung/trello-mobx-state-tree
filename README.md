===== TODOS (keep for reference) =====
- [X] Update card drag in react state
- [X] Move models into separate files
- [X] Update card drag via trello API
- [X] Enable CORS https://developers.trello.com/reference/#cors
- [X] Refactor (namings etc)
- [X] Add colors to console log (red for error, mediumaquamarine for success -- NOT GREEN for readability) 
- [X] Move tokens into .env file before pushing to git
- [X] Move all api calls to api/
- [X] Add a util for parsing a data-object into a query string
- [X] Style Navigation Links
- [X] Git push all the code transferred before completing layout (to avoid risk of irrevocable damage)
- [X] Layout on /board, where we flex the remaining height, with only the lists being scrollable
- [X] Make the entire List a droppable area
- [X] Add a util for FE logs (with color, path-string argument)
- [X] Add a list CTA + list model action.create
- [X] Add a card CTA + card model action.create
- [X] Add a generic button component (with options for colors etc to come later)
- [X] Fix container to allow overflow on List
- [X] Add custom style scrollbars
- [ ] Hover reveals star CTA, if starred, always show star (which acts as CTA to unstar)
- [ ] Add a generic context menu component where you wrap the toggle-button-child
- [ ] Add edit a List flow (use antd temporarily if necessary)
- [ ] Add edit a Card flow (use antd temporarily if necessary)
- [ ] Add a subnav bar
- [ ] Add a custom modal component (with its own route)
- [ ] Ensure refresh on the url actually loads the view
- [ ] Add error boundaries with "404 not found"
- [ ] Tidy up utils organisation (e.g. api should have its own utils?)
- [ ] Review the use of .toJSON()
- [ ] Remove constant dimensions (e.g. List width, List border radius, navbar/header heights etc into a constants file)

===== TODOS Laterbase =====

- [ ] Authentication
- [ ] Create layout divs + with slideout based on url!
- [ ] Move components to typescript and storybook
- [ ] Accessibility
- [ ] Internationalisation 
- [ ] Install hot module reloading - across 
- [ ] Performance improvements
- [ ] Release as 12 factor app with docker and NGINX

- [ ] Add super-funky color theme packages
- [ ] Add routing
- [ ] Add auth with localStorage
