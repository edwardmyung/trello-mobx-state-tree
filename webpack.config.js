const webpack = require("webpack");
const path = require("path");
const dotenv = require("dotenv");
const IS_DEV = process.env.NODE_ENV !== "production";

const HtmlWebpackPlugin = require("html-webpack-plugin");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = () => {
    const dotenvDevelopment = path.join(__dirname, ".env.development");
    const dotenvProduction = path.join(__dirname, ".env");
    const dotenvPath = IS_DEV ? dotenvDevelopment : dotenvProduction;

    // get the .env in root, parsed into an object
    const env = dotenv.config({path: dotenvPath}).parsed;

    // add prefix "process.env" to each of these keys
    const envKeys = Object.keys(env).reduce((prev, next) => {
        prev[`process.env.${next}`] = JSON.stringify(env[next]);
        return prev;
    }, {});

    return {
        // sets default path (e.g. for entry)
        context: path.resolve(__dirname, "./public"),

        // app root index.js
        entry: ["@babel/polyfill", path.resolve(__dirname, "./src/index.js")],

        // output destination of index.bundle.js and index.html
        output: {
            filename: "bundle.js",
            path: path.resolve(__dirname, "dist"),
            publicPath: "/"
        },

        plugins: [
            // adds the bundled js into html and outputs html into output.path
            new HtmlWebpackPlugin({
                template: path.join(__dirname, "./public/index.html")
            }),

            // removes dist folder whenever it rebuilds
            new CleanWebpackPlugin(),

            // Ensures css files are actual files on dist, not just loaded by JS on DOMContentLoaded
            new MiniCssExtractPlugin({
                filename: IS_DEV ? "[name].css" : "[name].[hash].css",
                chunkFilename: IS_DEV ? "[id].css" : "[id].[hash].css"
            }),

            // DefinePlugin ensures any passed in object is available in the global scope
            new webpack.DefinePlugin(envKeys)
        ],

        // config for webpack-dev-server (watches for file changes)
        devServer: {
            contentBase: path.resolve(__dirname, "dist/assets"), // where to store our content (e.g. images)
            open: true, // auto open browser to show
            compress: true, // activate gzip
            stats: "errors-only",
            port: 5001,
            historyApiFallback: true
        },

        module: {
            rules: [
                {
                    test: /\.(jpg|jpeg|png|gif|svg)$/,
                    use: {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]",
                            outputPath: "./src/assets/"
                        }
                    }
                },

                {
                    test: /\.(sc|sa|c)ss$/,
                    use: [
                        // order matters, but note last loader is run first
                        // ------------------------------------------------
                        // in prod we want requires to bring css into own files,
                        // we still need IS_DEV test to preserve Hot-Module-Replacement on dev
                        IS_DEV ? "style-loader" : MiniCssExtractPlugin.loader,
                        "css-loader", // allows require of .css files
                        "sass-loader", // allows require of .scss files
                        "postcss-loader" // calls the postcss.config.js in path-level of webpack.config.js
                    ]
                },

                // babel loaders
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env", "@babel/preset-react"],
                            plugins: ["@babel/plugin-syntax-object-rest-spread"]
                        }
                    }
                }
            ]
        }
    };
};
